<?php

namespace App\Constants;

class PermissionTitle
{
    const GET_ALL_S = 'GetAlls';
    const GET_ = 'Get';
    const CREATE_ = 'Create';
    const UPDATE_ = 'Update';
    const DELETE_ = 'Delete';

    const GET_ALL_USERS = 'GetAllUsers';
    const GET_USER = 'GetUser';
    const CREATE_USER = 'CreateUser';
    const UPDATE_USER = 'UpdateUser';
    const DELETE_USER = 'DeleteUser';

    const GET_ALL_ROLES = 'GetAllRoles';
    const GET_ROLE = 'GetRole';
    const CREATE_ROLE = 'CreateRole';
    const UPDATE_ROLE = 'UpdateRole';
    const DELETE_ROLE = 'DeleteRole';

    const GET_ALL_PERMISSIONS = 'GetAllPermissions';
    const GET_PERMISSION = 'GetPermission';
}
