<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

/**
 * @OA\Info(
 *      version="1.0.0",
 *      title="L5 OpenApi",
 *      description="L5 Swagger OpenApi description",
 *      @OA\Contact(
 *          email="tohidhabiby@gmail.com"
 *      ),
 *     @OA\License(
 *         name="Nginx"
 *     )
 * )
 *
 *  @OA\Server(
 *      url="http://localhost",
 *      description="L5 Swagger OpenApi dynamic host server"
 *  )
 *
 *  @OA\Server(
 *      url="http://localhost",
 *      description="L5 Swagger OpenApi Server"
 * )
 *
 * @OA\OpenApi(
 *   security={
 *     {
 *       "passport": {},
 *     }
 *   }
 * )
 *
 * @OA\Tag(
 *     name="Mobin",
 *     description="Everything about your Projects",
 *     @OA\ExternalDocumentation(
 *         description="Find out more",
 *         url="http://swagger.io"
 *     )
 * )
 */
class Controller extends BaseController
{
    use AuthorizesRequests;
    use DispatchesJobs;
    use ValidatesRequests;

    const DEFAULT_PAGE_SIZE = 10;
    const PER_PAGE = 'per_page';

    /**
     * @param Request $request BaseRequest.
     *
     * @return integer|mixed
     */
    protected function getPageSize(Request $request)
    {
        $pageSize = self::DEFAULT_PAGE_SIZE;
        if ($request->has(self::PER_PAGE) && !empty($request->get(self::PER_PAGE))) {
            $pageSize = $request->get(self::PER_PAGE);
        }
        return $pageSize;
    }
}
