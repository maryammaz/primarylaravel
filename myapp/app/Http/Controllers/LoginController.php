<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    /**
     * @param LoginRequest $request Loging Request.
     *
     * @return JsonResponse Json Response.
     */
    public function login(LoginRequest $request)
    {
        $user = User::whereMobileIs($request->get(User::MOBILE))->first();
        if ($user) {
            if (Hash::check($request->get(User::PASSWORD), $user->getPassword())) {
                $token = $user->createToken('new user')->accessToken;
                $response = [
                    'token' => $token,
                    'user' => $user
                ];
                return response()->json(['data' => $response]);
            }
        }
        $response = ['message' => 'User or Password is incorrect!'];

        return response()->json($response, Response::HTTP_UNAUTHORIZED);
    }

    /**
     * @param Request $request Login Request.
     *
     * @return JsonResponse Json Response.
     */
    public function logout(Request $request)
    {
        $token = $request->user()->token();
        $token->revoke();

        return response()->json();
    }
}
