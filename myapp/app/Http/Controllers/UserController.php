<?php

namespace App\Http\Controllers;

use App\Constants\PermissionTitle;
use App\Filters\UserFilter;
use App\Http\Requests\UserRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @OA\Get(
     *     path="/api/users",
     *     tags={"Users"},
     *     security={{"passport": {}}},
     *     operationId="GetAllUsers",
     *     description="Get All Users.",
     *     @OA\Parameter(
     *         description="First name for filter user by first name like",
     *         in="query",
     *         name="first_name",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         description="Last name for filter user by last name like",
     *         in="query",
     *         name="last_name",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         description="National Code for filter user by national code like",
     *         in="query",
     *         name="nationalCode",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         description="City for filter user by city like",
     *         in="query",
     *         name="city",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         description="Region for filter user by region like",
     *         in="query",
     *         name="region",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         description="Type for filter user by type like",
     *         in="query",
     *         name="type",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         description="Email for filter user by email like",
     *         in="query",
     *         name="email",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         description="Mobile for filter user by mobile is",
     *         in="query",
     *         name="mobile",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         description="IDs for filter user by IDs",
     *         in="query",
     *         name="ids",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         description="Approved for filter player by approved",
     *         in="query",
     *         name="approved",
     *         required=false,
     *         @OA\Schema(type="boolean")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="All Users response",
     *         @OA\JsonContent(ref="#/components/schemas/Users")
     *     )
     * )
     *
     * @param UserFilter $filters Filter.
     * @param Request    $request Request.
     *
     * @return AnonymousResourceCollection
     */
    public function index(UserFilter $filters, Request $request): AnonymousResourceCollection
    {
        return UserResource::collection(User::filter($filters)->paginate($this->getPageSize($request)));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @OA\Post(
     *     path="/api/users",
     *     tags={"Users"},
     *     security={{"passport": {}}},
     *     operationId="CreateUser",
     *     description="Create a new User.",
     *     @OA\RequestBody(
     *         description="User to add",
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(ref="#/components/schemas/UserRequest")
     *         )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="User response",
     *         @OA\JsonContent(ref="#/components/schemas/User")
     *     )
     * )
     *
     * @param UserRequest $request BaseRequest.
     *
     * @return UserResource
     */
    public function store(UserRequest $request): UserResource
    {
        $user = User::createObject(
            $request->get(User::NATIONAL_CODE),
            $request->get(User::FIRST_NAME),
            $request->get(User::MOBILE),
            $request->get(User::TYPE),
            $request->get(User::LAST_NAME),
            $request->get(User::APPROVED),
            $request->get(User::PASSWORD),
            $request->get(User::ADDRESS),
            $request->get(User::CITY),
            $request->get(User::EMAIL),
            $request->get(User::CHARGE),
            $request->get(User::SUBSCRIPTION_EXPIRE),
            $request->get(User::REGION),
            $request->get(User::PHONE)
        );

        $user->roles()->sync($request->get('roles'));

        return new UserResource($user);
    }

    /**
     * Display the specified resource.
     *
     * @OA\Get(
     *     path="/api/users/{user_id}",
     *     tags={"Users"},
     *     security={{"passport": {}}},
     *     operationId="GetUser",
     *     description="Get User.",
     *     @OA\Parameter(
     *         description="User id which is going to fetch",
     *         in="path",
     *         name="user_id",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="User response",
     *         @OA\JsonContent(ref="#/components/schemas/User")
     *     )
     * )
     *
     * @param User $user User.
     *
     * @return UserResource
     */
    public function show(User $user): UserResource
    {
        return new UserResource($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @OA\Patch(
     *     path="/api/users/{user_id}",
     *     tags={"Users"},
     *     security={{"passport": {}}},
     *     operationId="UpdateUser",
     *     description="Update a new User.",
     *     @OA\Parameter(
     *         description="User id which is going to update",
     *         in="path",
     *         name="user_id",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64")
     *     ),
     *     @OA\RequestBody(
     *         description="User to update",
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(ref="#/components/schemas/UserRequest")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="User response",
     *         @OA\JsonContent(ref="#/components/schemas/User")
     *     )
     * )
     *
     * @param UserRequest $request BaseRequest.
     * @param User        $user    User.
     *
     * @return UserResource
     */
    public function update(UserRequest $request, User $user): UserResource
    {
        $user->updateObject(
            $request->get(User::NATIONAL_CODE),
            $request->get(User::FIRST_NAME),
            $request->get(User::MOBILE),
            $request->get(User::TYPE),
            $request->get(User::LAST_NAME),
            $request->get(User::APPROVED),
            $request->get(User::PASSWORD),
            $request->get(User::ADDRESS),
            $request->get(User::CITY),
            $request->get(User::EMAIL),
            $request->get(User::CHARGE),
            $request->get(User::SUBSCRIPTION_EXPIRE),
            $request->get(User::REGION),
            $request->get(User::PHONE)
        );

        $user->roles()->sync($request->get('roles'));

        return new UserResource($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @OA\Delete(
     *     path="/api/users/{user_id}",
     *     tags={"Users"},
     *     security={{"passport": {}}},
     *     operationId="DeleteUser",
     *     description="Delete User.",
     *     @OA\Parameter(
     *         description="User id which is going to fetch",
     *         in="path",
     *         name="user_id",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="User response",
     *         @OA\JsonContent(ref="#/components/schemas/User")
     *     )
     * )
     *
     * @param User $user User.
     *
     * @return JsonResponse
     */
    public function destroy(User $user): JsonResponse
    {
        try {
            $user->delete();

            return response()->json([], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return response()->json(['message' => $exception->getMessage()], Response::HTTP_CONFLICT);
        }
    }
}
