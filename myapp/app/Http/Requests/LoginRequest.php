<?php

namespace App\Http\Requests;

use App\Models\User;

class LoginRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            User::PASSWORD => 'required|string',
            User::MOBILE => 'required|numeric',
        ];
    }
}
