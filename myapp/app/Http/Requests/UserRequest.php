<?php

namespace App\Http\Requests;

use App\Models\User;

/**
 * Class UserRequest
 * @package App\Http\Requests
 *
 * @OA\Schema(
 *   schema="UserRequest",
 *   type="object",
 *   required={"mobile", "national_code", "first_name", "type"},
 *   @OA\Property(property="mobile", type="string"),
 *   @OA\Property(property="first_name", type="string"),
 *   @OA\Property(property="last_name", type="string"),
 *   @OA\Property(property="approved", type="boolean"),
 *   @OA\Property(property="city", type="string"),
 *   @OA\Property(property="region", type="string"),
 *   @OA\Property(property="password", type="string"),
 *   @OA\Property(property="email", type="string"),
 *   @OA\Property(property="type", type="string"),
 *   @OA\Property(property="national_code", type="string"),
 *   @OA\Property(property="address", type="string"),
 *   @OA\Property(property="phone", type="string"),
 *   @OA\Property(property="charge", type="integer"),
 *   @OA\Property(property="subscription_date", type="string"),
 * )
 */
class UserRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            User::NATIONAL_CODE => sprintf(
                'required|max:11|unique:%s,%s,%s',
                User::TABLE,
                User::NATIONAL_CODE,
                optional($this->user)->{User::ID}
            ),
            User::APPROVED => 'nullable|boolean',
            User::EMAIL => sprintf(
                'nullable|unique:%s,%s,%s',
                User::TABLE,
                User::EMAIL,
                optional($this->user)->{User::ID}
            ),
            User::MOBILE => sprintf(
                'required|unique:%s,%s,%s',
                User::TABLE,
                User::MOBILE,
                optional($this->user)->{User::ID}
            ),
            User::FIRST_NAME => 'required|string|max:150',
            User::LAST_NAME => 'nullable|string',
            User::TYPE => 'required|in:' . implode(',', User::$types),
            User::CHARGE => 'nullable|numeric',
            User::SUBSCRIPTION_EXPIRE => 'nullable|date',
            User::PASSWORD => 'nullable',
            User::CITY => 'nullable|string|max:150',
            User::REGION => 'nullable|string|max:100',
            User::ADDRESS => 'nullable|string',
            User::PHONE => 'nullable|max:13',
        ];
    }
}
