<?php

namespace App\Interfaces\Models;

use App\Filters\RoleFilter;
use App\Interfaces\Traits\HasTitleInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

interface RoleInterface extends HasTitleInterface
{
    /**
     * Filter scope.
     *
     * @param Builder    $builder Builder.
     * @param RoleFilter $filters Filters.
     *
     * @return Builder
     */
    public function scopeFilter(Builder $builder, RoleFilter $filters): Builder;

    /**
     * @return BelongsToMany
     */
    public function users(): BelongsToMany;

    /**
     * Create new Role.
     *
     * @param string $title Title.
     *
     * @return RoleInterface
     */
    public static function createObject(string $title): RoleInterface;

    /**
     * Update role.
     *
     * @param string $title Title.
     *
     * @return RoleInterface
     */
    public function updateObject(string $title): RoleInterface;
}
