<?php

namespace App\Interfaces\Models;

use App\Interfaces\Traits\HasApprovedInterface;
use App\Interfaces\Traits\HasEmailInterface;
use App\Interfaces\Traits\HasFirstNameInterface;
use App\Interfaces\Traits\HasIdInterface;
use App\Interfaces\Traits\HasLastNameInterface;
use App\Interfaces\Traits\HasMobileInterface;
use App\Interfaces\Traits\HasNationalCodeInterface;
use App\Interfaces\Traits\HasTypeInterface;
use App\Interfaces\Traits\HasCityInterface;
use App\Interfaces\Traits\HasRegionInterface;
use Illuminate\Database\Eloquent\Builder;
use phpDocumentor\Reflection\Types\Integer;

interface UserInterface extends
    HasFirstNameInterface,
    HasLastNameInterface,
    HasNationalCodeInterface,
    HasTypeInterface,
    HasEmailInterface,
    HasMobileInterface,
    HasApprovedInterface,
    HasCityInterface,
    HasRegionInterface,
    HasIdInterface
{
    /**
     * Create new user.
     *
     * @param string       $nationalCode        National code.
     * @param string       $firstName           First name.
     * @param string       $mobile              Mobile.
     * @param string       $type                Type of users (Driver, Company, Agent).
     * @param string|null  $lastName            Last name.
     * @param boolean|null $approved            The operator must accept the new user.
     * @param string|null  $password            Password.
     * @param string|null  $address             Address.
     * @param string|null  $city                City.
     * @param string|null  $email               Email.
     * @param string|null  $charge              Charge for using Mobin panel.
     * @param string|null  $subscription_expire Show end day of subscription.
     * @param string|null  $region              Region.
     * @param string|null  $phone               Phone Number.
     *
     * @return UserInterface
     */
    public static function createObject(
        string $nationalCode,
        string $firstName,
        string $mobile,
        string $type,
        ?string $lastName,
        ?bool $approved,
        ?string $password,
        ?string $address,
        ?string $city,
        ?string $email,
        ?string $charge,
        ?string $subscription_expire,
        ?string $region,
        ?string $phone
    );

    /**
     * @param string       $nationalCode        National Code.
     * @param string       $firstName           First Name.
     * @param string       $mobile              Mobile.
     * @param string       $type                Type.
     * @param string|null  $lastName            Last Name.
     * @param boolean|null $approved            Approved.
     * @param string|null  $password            Password.
     * @param string|null  $address             Address.
     * @param string|null  $city                City.
     * @param string|null  $email               Email.
     * @param string|null  $charge              Charge.
     * @param string|null  $subscription_expire Subscription Expire.
     * @param string|null  $region              Region.
     * @param string|null  $phone               Phone.
     * @return mixed
     */
    public function updateObject(
        string $nationalCode,
        string $firstName,
        string $mobile,
        string $type,
        ?string $lastName,
        ?bool $approved,
        ?string $password,
        ?string $address,
        ?string $city,
        ?string $email,
        ?string $charge,
        ?string $subscription_expire,
        ?string $region,
        ?string $phone
    );

    /**
     * @param string $permission Permission.
     *
     * @return boolean
     */
    public function hasPermission(string $permission): bool;

    /**
     * @param Builder $builder    Builder.
     * @param string  $permission Permission.
     *
     * @return Builder
     */
    public function scopeWhereHasPermission(Builder $builder, string $permission): Builder;
}
