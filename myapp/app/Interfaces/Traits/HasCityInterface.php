<?php

namespace App\Interfaces\Traits;

use Illuminate\Database\Eloquent\Builder;

interface HasCityInterface
{
    /**
     * @param Builder $builder Builder.
     * @param string  $city    City.
     *
     * @return Builder
     */
    public function scopeWhereCityLike(Builder $builder, string $city): Builder;
}
