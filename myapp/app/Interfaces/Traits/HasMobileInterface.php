<?php

namespace App\Interfaces\Traits;

use Illuminate\Database\Eloquent\Builder;

interface HasMobileInterface
{
    /**
     * @param Builder $builder Builder.
     * @param string  $mobile  Mobile.
     *
     * @return Builder
     */
    public function scopeWhereMobileIs(Builder $builder, string $mobile): Builder;
}
