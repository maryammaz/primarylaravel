<?php

namespace App\Interfaces\Traits;

use Illuminate\Database\Eloquent\Builder;

interface HasNationalCodeInterface
{
    /**
     * @param Builder $builder      Builder.
     * @param string  $nationalCode National Code.
     *
     * @return Builder
     */
    public function scopeWhereNationalCodeLike(Builder $builder, string $nationalCode): Builder;
}
