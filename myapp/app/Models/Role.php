<?php

namespace App\Models;

use App\Filters\RoleFilter;
use App\Interfaces\Models\RoleInterface;
use App\Traits\HasTitleTrait;
use App\Traits\MagicMethodsTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Role extends Model implements RoleInterface
{
    use HasFactory;
    use HasTitleTrait;
    use MagicMethodsTrait;

    const TABLE = 'roles';
    const ID = 'id';
    const TITLE = 'title';
    const CUSTOM_ROLE = 'admin';

    /**
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Filter scope.
     *
     * @param Builder    $builder Builder.
     * @param RoleFilter $filters Filters.
     *
     * @return Builder
     */
    public function scopeFilter(Builder $builder, RoleFilter $filters): Builder
    {
        return $filters->apply($builder);
    }

    /**
     * @return BelongsToMany
     */
    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class);
    }

    /**
     * Create new Role.
     *
     * @param string $title Title.
     *
     * @return RoleInterface
     */
    public static function createObject(string $title): RoleInterface
    {
        $role = new self();
        $role->setTitle($title);
        $role->save();

        return $role;
    }

    /**
     * Update role.
     *
     * @param string $title Title.
     *
     * @return RoleInterface
     */
    public function updateObject(string $title): RoleInterface
    {
        $this->setTitle($title);
        $this->save();

        return $this;
    }

    /**
     * @return BelongsToMany
     */
    public function permissions(): BelongsToMany
    {
        return $this->belongsToMany(Permission::class);
    }
}
