<?php

namespace App\Models;

use App\Filters\UserFilter;
use App\Interfaces\Models\UserInterface;
use App\Traits\HasCityTrait;
use App\Traits\HasApprovedTrait;
use App\Traits\HasEmailTrait;
use App\Traits\HasIdTrait;
use App\Traits\HasMobileTrait;
use App\Traits\HasNationalCodeTrait;
use App\Traits\HasFirstNameTrait;
use App\Traits\HasLastNameTrait;
use App\Traits\HasRegionTrait;
use App\Traits\HasTypeTrait;
use App\Traits\MagicMethodsTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable implements UserInterface
{
    use HasFactory;
    use Notifiable;
    use HasApiTokens;
    use HasFirstNameTrait;
    use HasLastNameTrait;
    use MagicMethodsTrait;
    use HasNationalCodeTrait;
    use HasTypeTrait;
    use HasEmailTrait;
    use HasMobileTrait;
    use HasApprovedTrait;
    use HasCityTrait;
    use HasRegionTrait;
    use HasIdTrait;

    const TABLE = 'users';
    const ID = 'id';
    const NATIONAL_CODE = 'national_code';
    const APPROVED = 'approved';
    const EMAIL = 'email';
    const MOBILE = 'mobile';
    const FIRST_NAME = 'first_name';
    const LAST_NAME = 'last_name';
    const TYPE = 'type';
    const CHARGE = 'charge';
    const SUBSCRIPTION_EXPIRE = 'subscription_expire';
    const PASSWORD = 'password';
    const CITY = 'city';
    const REGION = 'region';
    const ADDRESS = 'address';
    const PHONE = 'phone';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    const TYPE_DRIVER = 'driver';
    const TYPE_COMPANY = 'company';
    const TYPE_AGENT = 'agent';

    /**
     * @var string[]
     */
    public static $types = [
        self::TYPE_DRIVER,
        self::TYPE_COMPANY,
        self::TYPE_AGENT,
    ];

    /**
     * Filter scope.
     *
     * @param Builder    $builder Builder.
     * @param UserFilter $filters Filters.
     *
     * @return Builder
     */
    public function scopeFilter(Builder $builder, UserFilter $filters): Builder
    {
        return $filters->apply($builder);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::ID,
        self::NATIONAL_CODE,
        self::APPROVED,
        self::FIRST_NAME,
        self::LAST_NAME,
        self::TYPE,
        self::EMAIL,
        self::CHARGE,
        self::SUBSCRIPTION_EXPIRE,
        self::PASSWORD,
        self::CITY,
        self::ADDRESS,
        self::PHONE
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        self::PASSWORD
    ];

    /**
     * Create new user.
     *
     * @param string       $nationalCode        National code.
     * @param string       $firstName           First name.
     * @param string       $mobile              Mobile.
     * @param string       $type                Type of users (Driver, Company, Agent).
     * @param string|null  $lastName            Last name.
     * @param boolean|null $approved            The operator must accept the new user.
     * @param string|null  $password            Password.
     * @param string|null  $address             Address.
     * @param string|null  $city                City.
     * @param string|null  $email               Email.
     * @param string|null  $charge              Charge for using Mobin panel.
     * @param string|null  $subscription_expire Show end day of subscription.
     * @param string|null  $region              Region.
     * @param string|null  $phone               Phone Number.
     *
     * @return UserInterface
     */
    public static function createObject(
        string $nationalCode,
        string $firstName,
        string $mobile,
        string $type,
        ?string $lastName,
        ?bool $approved,
        ?string $password,
        ?string $address,
        ?string $city,
        ?string $email,
        ?string $charge,
        ?string $subscription_expire,
        ?string $region,
        ?string $phone
    ): UserInterface {
        $user = new static();
        $user->setNationalCode($nationalCode);
        $user->setApproved((bool)$approved);
        $user->setFirstName($firstName);
        $user->setLastName($lastName);
        $user->setMobile($mobile);
        $user->setType($type);
        $user->setPassword(Hash::make($password));
        $user->setAddress($address);
        $user->setCity($city);
        $user->setEmail($email);
        if (!is_null($charge)) {
            $user->setCharge($charge);
        }
        $user->setSubscriptionExpire($subscription_expire);
        $user->setRegion($region);
        $user->setPhone($phone);
        $user->save();

        return $user;
    }

    /**
     * update existing user.
     *
     * @param string       $nationalCode        NationalCode.
     * @param string       $firstName           FirstName.
     * @param string       $mobile              Mobile.
     * @param string       $type                Type.
     * @param string|null  $lastName            LastName.
     * @param boolean|null $approved            Approved.
     * @param string|null  $password            Password.
     * @param string|null  $address             Address.
     * @param string|null  $city                City.
     * @param string|null  $email               Email.
     * @param string|null  $charge              Charge.
     * @param string|null  $subscription_expire SubscriptionExpire.
     * @param string|null  $region              Region.
     * @param string|null  $phone               Phone.
     *
     * @return UserInterface
     */
    public function updateObject(
        string $nationalCode,
        string $firstName,
        string $mobile,
        string $type,
        ?string $lastName,
        ?bool $approved,
        ?string $password,
        ?string $address,
        ?string $city,
        ?string $email,
        ?string $charge,
        ?string $subscription_expire,
        ?string $region,
        ?string $phone
    ): UserInterface {
        $this->setNationalCode($nationalCode);
        $this->setApproved((bool)$approved);
        $this->setFirstName($firstName);
        $this->setLastName($lastName);
        $this->setMobile($mobile);
        $this->setType($type);
        $this->setPassword($password);
        $this->setAddress($address);
        $this->setCity($city);
        $this->setEmail($email);
        if (!is_null($charge)) {
            $this->setCharge($charge);
        }
        $this->setSubscriptionExpire($subscription_expire);
        $this->setRegion($region);
        $this->setPhone($phone);
        $this->save();

        return $this;
    }

    /**
     * @return BelongsToMany
     */
    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     * User has permission.
     *
     * @param string $permission Permission title.
     *
     * @return boolean
     */
    public function hasPermission(string $permission): bool
    {
        return !empty(static::whereIdIn([$this->getId()])->whereHasPermission($permission)->first());
    }

    /**
     * @param Builder $builder    Builder.
     * @param string  $permission Permission title.
     *
     * @return Builder
     */
    public function scopeWhereHasPermission(Builder $builder, string $permission): Builder
    {
        return $builder->whereHas('roles', function (Builder $joinRole) use ($permission) {
            return $joinRole->whereHas('permissions', function (Builder $joinPermission) use ($permission) {
                return $joinPermission->whereTitleIs($permission);
            });
        });
    }
}
