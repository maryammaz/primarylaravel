<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;

trait HasCityTrait
{
    /**
     * @param Builder $builder Builder.
     * @param string  $city    City.
     *
     * @return Builder
     */
    public function scopeWhereCityLike(Builder $builder, string $city): Builder
    {
        return $builder->where(self::CITY, 'like', "%$city%");
    }
}
