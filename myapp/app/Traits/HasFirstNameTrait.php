<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;

trait HasFirstNameTrait
{
    /**
     * @param Builder $builder   Builder.
     * @param string  $firstName FirstName.
     *
     * @return Builder
     */
    public function scopeWhereFirstNameLike(Builder $builder, string $firstName): Builder
    {
        return $builder->where(self::FIRST_NAME, 'like', "%$firstName%");
    }
}
