<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;

trait HasNationalCodeTrait
{
    /**
     * @param Builder $builder      Builder.
     * @param string  $nationalCode National Code.
     * @return Builder
     */
    public function scopeWhereNationalCodeLike(Builder $builder, string $nationalCode): Builder
    {
        return $builder->where(self::NATIONAL_CODE, 'like', "%$nationalCode%");
    }
}
