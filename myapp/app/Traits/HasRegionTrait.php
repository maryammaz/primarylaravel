<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;

trait HasRegionTrait
{
    /**
     * @param Builder $builder Builder.
     * @param string  $region  Region.
     *
     * @return Builder
     */
    public function scopeWhereRegionLike(Builder $builder, string $region): Builder
    {
        return $builder->where(self::REGION, 'like', "%$region%");
    }
}
