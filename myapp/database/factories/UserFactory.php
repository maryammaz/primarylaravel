<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            User::NATIONAL_CODE => (string)$this->faker->unique()->randomDigit,
            User::APPROVED => $this->faker->boolean,
            User::MOBILE => (string)rand(10000000000, 999999999999),
            User::FIRST_NAME => $this->faker->firstName,
            User::LAST_NAME => $this->faker->lastName,
            User::EMAIL => $this->faker->email,
            User::CHARGE => (string)$this->faker->randomDigit,
            User::SUBSCRIPTION_EXPIRE => $this->faker->date(),
            User::PASSWORD => bcrypt('123'),
            User::CITY => $this->faker->city,
            User::REGION => $this->faker->countryISOAlpha3,
            User::ADDRESS => $this->faker->address,
            User::PHONE => (string)rand(10000000000, 999999999999),
            User::TYPE => User::$types[array_rand(User::$types)],
        ];
    }
}
