<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(User::TABLE, function (Blueprint $table) {
            $table->id();
            $table->string(User::NATIONAL_CODE, 11)->unique();
            $table->boolean(User::APPROVED)->default(false);
            $table->string(User::FIRST_NAME, 150);
            $table->string(User::LAST_NAME, 200)->nullable();
            $table->string(User::MOBILE,13)->unique();
            $table->enum(User::TYPE, User::$types)->comment(implode(',', User::$types));
            $table->string(User::PASSWORD)->nullable();
            $table->text(User::ADDRESS)->nullable();
            $table->string(User::CITY, 150)->nullable();
            $table->string(User::EMAIL, 150)->nullable()->unique();
            $table->string(User::CHARGE)->default(0);
            $table->date(User::SUBSCRIPTION_EXPIRE)->nullable();
            $table->string(User::REGION, 100)->nullable();
            $table->string(User::PHONE, 13)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
