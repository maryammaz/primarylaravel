<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::firstOrCreate([Role::TITLE => Role::CUSTOM_ROLE]);
        $user = User::first();
        $user->roles()->sync([Role::whereTitleLike(Role::CUSTOM_ROLE)->first()->getId()]);
    }
}
