<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        User::query()->firstOrCreate(
            [
                User::NATIONAL_CODE => '0010899979',
            ],
            [
                User::NATIONAL_CODE => '0010899979',
                User::FIRST_NAME => 'John',
                User::MOBILE => '09346879584',
                User::TYPE => User::TYPE_COMPANY,
                User::LAST_NAME => 'Doe',
                User::APPROVED => 1,
                User::PASSWORD => bcrypt(123),
                User::ADDRESS => 'street Azadi',
                User::CITY => 'Tehran',
                User::EMAIL => 'john@doe.com',
                User::CHARGE => '10000000',
                User::SUBSCRIPTION_EXPIRE => '2020/10/05',
                User::REGION => 'Tehran',
                User::PHONE => '0218749976',
            ]
        );
    }
}
