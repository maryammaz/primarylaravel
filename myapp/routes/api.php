<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['cors']], function () {
    Route::post('/login', 'LoginController@login')->name('login');
    Route::middleware('auth:api')->get('logout', 'LoginController@logout');
    Route::group(['middleware' => ['auth:api', 'permission']], function () {
        Route::apiResource('users', 'UserController');
        Route::apiResource('roles', 'RoleController');
        Route::apiResource('permissions', 'PermissionController')->only(['index', 'show']);
        Route::apiResource('packing_types', 'PackingTypeController');
    });
});
