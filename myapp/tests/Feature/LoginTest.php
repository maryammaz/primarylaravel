<?php

namespace Tests\Feature;

use App\Models\User;
use DateTime;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\ClientRepository;
use Tests\TestCase;

class LoginTest extends TestCase
{
    /**
     * @return void
     *
     * @throws \Exception
     */
    protected function createToken(){
        $clientRepository = new ClientRepository();
        $client = $clientRepository->createPersonalAccessClient(
            null, 'Test Personal Access Client', 'http://localhost'
        );

        DB::table('oauth_personal_access_clients')->insert([
            'client_id' => $client->id,
            'created_at' => new DateTime,
            'updated_at' => new DateTime,
        ]);
    }

    /**
     * @test
     */
    public function login()
    {
        $this->createToken();

        $user = User::factory()->create();

        $response = $this->postJson(
            '/api/login',
            [
                User::MOBILE => $user->getMobile(),
                User::PASSWORD => '123'
            ]);

        $response->assertOk();
        $this->assertTrue(isset($response->json()['data']['token']));
        $this->assertTrue(isset($response->json()['data']['user']));
        $this->assertEquals($response->json()['data']['user'][User::ID], $user->getId());
    }

    /**
     * @test
     */
    public function userCannotLoginWithIncorrectPassword()
    {
        $this->createToken();

        $user = User::factory()->create();

        $response = $this->postJson(
            'api/login',
            [
                User::MOBILE => $user->getMobile(),
                User::PASSWORD => null,
        ]);

        $this->assertArrayHasKey(User::PASSWORD, $response->json());
    }


    /**
     * @test
     */
    public function userCannotLoginWithIncorrectUser()
    {
        $user = User::factory()->create(
            [
                User::MOBILE =>'9125788697',
            ]);

        $response = $this->postJson('api/login',
        [
            User::MOBILE => null,
            User::PASSWORD => bcrypt('123'),
        ]);

        $this->assertArrayHasKey(User::MOBILE, $response->json());
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * @test
     */
    public function userCanLoggedOut()
    {
        $this->actingAsUser();
        $this->getJson('/api/logout')->assertOk();
    }
}
