<?php

namespace Tests\Feature;

use App\Constants\PermissionTitle;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class UserTest extends TestCase
{
    /**
     * @test
     */
    public function createUser()
    {
        $this->actingAsUserWithPermission(PermissionTitle::CREATE_USER);
        $user = User::factory()->make();
        $response = $this->postJson(
            'api/users',
            [
                User::NATIONAL_CODE => $user->{User::NATIONAL_CODE},
                User::FIRST_NAME => $user->{User::FIRST_NAME},
                User::LAST_NAME => $user->{User::LAST_NAME},
                User::MOBILE => $user->{User::MOBILE},
                User::TYPE => $user->{User::TYPE},
                User::PASSWORD => $user->{User::PASSWORD},
                User::ADDRESS => $user->{User::ADDRESS},
                User::CITY => $user->{User::CITY},
                User::EMAIL => $user->{User::EMAIL},
                User::CHARGE => $user->{User::CHARGE},
                User::SUBSCRIPTION_EXPIRE => $user->{User::SUBSCRIPTION_EXPIRE},
                User::REGION => $user->{User::REGION},
                User::PHONE => $user->{User::PHONE},
                User::APPROVED => $user->{User::APPROVED},
            ]
        );

        $response->assertCreated();
        $this->assertDatabaseHas(User::TABLE, $user->toArray());
    }

    /**
     * @test
     */
    public function createUserAndAssignRoles()
    {
        $this->actingAsUserWithPermission(PermissionTitle::CREATE_USER);
        $user = User::factory()->make();
        $role = Role::factory()->create();
        $response = $this->postJson(
            'api/users',
            [
                User::NATIONAL_CODE => $user->{User::NATIONAL_CODE},
                User::FIRST_NAME => $user->{User::FIRST_NAME},
                User::LAST_NAME => $user->{User::LAST_NAME},
                User::MOBILE => $user->{User::MOBILE},
                User::TYPE => $user->{User::TYPE},
                User::PASSWORD => $user->{User::PASSWORD},
                User::ADDRESS => $user->{User::ADDRESS},
                User::CITY => $user->{User::CITY},
                User::EMAIL => $user->{User::EMAIL},
                User::CHARGE => $user->{User::CHARGE},
                User::SUBSCRIPTION_EXPIRE => $user->{User::SUBSCRIPTION_EXPIRE},
                User::REGION => $user->{User::REGION},
                User::PHONE => $user->{User::PHONE},
                User::APPROVED => $user->{User::APPROVED},
                'roles' => [$role->getId()],
            ]
        );

        $response->assertCreated();
        $this->assertTrue($response->getOriginalContent()->roles->contains($role));
    }

    /**
     * @test
     */
    public function userWithoutPermissionCanNotCreate()
    {
        $this->actingAsUser();
        $user = User::factory()->make();
        $this->postJson(
            'api/users',
            [
                User::NATIONAL_CODE => $user->{User::NATIONAL_CODE},
                User::FIRST_NAME => $user->{User::FIRST_NAME},
                User::LAST_NAME => $user->{User::LAST_NAME},
                User::MOBILE => $user->{User::MOBILE},
                User::TYPE => $user->{User::TYPE},
                User::PASSWORD => $user->{User::PASSWORD},
                User::ADDRESS => $user->{User::ADDRESS},
                User::CITY => $user->{User::CITY},
                User::EMAIL => $user->{User::EMAIL},
                User::CHARGE => $user->{User::CHARGE},
                User::SUBSCRIPTION_EXPIRE => $user->{User::SUBSCRIPTION_EXPIRE},
                User::REGION => $user->{User::REGION},
                User::PHONE => $user->{User::PHONE},
                User::APPROVED => $user->{User::APPROVED}
            ]
        )->assertForbidden();
    }

    /**
     * @test
     */
    public function UserWithoutLoggedInCanNotCreateUser()
    {
        $user = User::factory()->make();
        $response = $this->postJson(
            'api/users',
            [
                User::NATIONAL_CODE => $user->{User::NATIONAL_CODE},
                User::FIRST_NAME => $user->{User::FIRST_NAME},
                User::LAST_NAME => $user->{User::LAST_NAME},
                User::MOBILE => $user->{User::MOBILE},
                User::TYPE => $user->{User::TYPE},
                User::PASSWORD => $user->{User::PASSWORD},
                User::ADDRESS => $user->{User::ADDRESS},
                User::CITY => $user->{User::CITY},
                User::EMAIL => $user->{User::EMAIL},
                User::CHARGE => $user->{User::CHARGE},
                User::SUBSCRIPTION_EXPIRE => $user->{User::SUBSCRIPTION_EXPIRE},
                User::REGION => $user->{User::REGION},
                User::PHONE => $user->{User::PHONE},
                User::APPROVED => $user->{User::APPROVED}
            ]
        )->assertUnauthorized();
    }

    /**
     * @test
     */
    public function canNotCreateUserWithInvalidParameters()
    {
        $this->actingAsUserWithPermission(PermissionTitle::CREATE_USER);
        $response = $this->postJson(
            'api/users',
            [
                User::NATIONAL_CODE => null,
                User::FIRST_NAME => null,
                User::MOBILE => null,
                User::TYPE => null
            ]
        );
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $this->assertArrayHasKey(User::NATIONAL_CODE, $response->json());
        $this->assertArrayHasKey(User::FIRST_NAME, $response->json());
        $this->assertArrayHasKey(User::MOBILE, $response->json());
        $this->assertArrayHasKey(User::TYPE, $response->json());

        $user = User::factory()->create();
        $response = $this->postJson(
            route('users.store'),
            [
                User::NATIONAL_CODE => $user->{User::NATIONAL_CODE},
                User::MOBILE => $user->{User::MOBILE},
                User::EMAIL => $user->{User::EMAIL},
                User::CHARGE => $this->faker->title,
                User::FIRST_NAME => $this->faker->paragraphs,
                User::SUBSCRIPTION_EXPIRE => $this->faker->title,
                User::CITY => $this->faker->paragraphs,
                User::REGION => $this->faker->paragraphs,
            ]
        );
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $this->assertArrayHasKey(User::NATIONAL_CODE, $response->json());
        $this->assertArrayHasKey(User::MOBILE, $response->json());
        $this->assertArrayHasKey(User::EMAIL, $response->json());
        $this->assertArrayHasKey(User::CHARGE, $response->json());
        $this->assertArrayHasKey(User::FIRST_NAME, $response->json());
        $this->assertArrayHasKey(User::SUBSCRIPTION_EXPIRE, $response->json());
        $this->assertArrayHasKey(User::CITY, $response->json());
        $this->assertArrayHasKey(User::REGION, $response->json());
    }

    /**
     * @test
     */
    public function getUser()
    {
        $user = User::factory()->create();
        $this->actingAsUserWithPermission(PermissionTitle::GET_USER);
        $response = $this->getJson(route('users.show', $user));
        $response->assertOk();
        $this->assertTrue($response->getOriginalContent()->is($user));
    }

    /**
     * @test
     */
    public function userWithoutPermissionCanNotGetUser()
    {
        $user = User::factory()->create();
        $this->actingAsUser();
        $this->getJson(route('users.show', $user))->assertForbidden();
    }

    /**
     * @test
     */
    public function userWithoutLoggedInCanNotGetUser()
    {
        $user = User::factory()->create();
        $this->getJson(route('users.show', $user))->assertUnauthorized();
    }

    /**
     * @test
     */
    public function updateUser()
    {
        $this->actingAsUserWithPermission(PermissionTitle::UPDATE_USER);
        $nationalCode = $this->faker->unique()->randomDigit;
        $firstName = $this->faker->firstName;
        $mobile = rand(10000000000, 999999999999);
        $type = User::$types[array_rand(User::$types)];

        $user = User::factory()->create();
        $response = $this->patchJson(
            route('users.update', $user),
            [
                User::NATIONAL_CODE => $nationalCode,
                User::FIRST_NAME => $firstName,
                User::MOBILE => $mobile,
                User::TYPE => $type,
            ]
        );
        $response->assertOk();
        $this->assertEquals($nationalCode, $response->getOriginalContent()->{User::NATIONAL_CODE});
        $this->assertEquals($firstName, $response->getOriginalContent()->{User::FIRST_NAME});
        $this->assertEquals($mobile, $response->getOriginalContent()->{User::MOBILE});
        $this->assertEquals($type, $response->getOriginalContent()->{User::TYPE});
    }

    /**
     * @test
     */
    public function updateUserAndAssingRole()
    {
        $this->actingAsUserWithPermission(PermissionTitle::UPDATE_USER);
        $nationalCode = $this->faker->unique()->randomDigit;
        $firstName = $this->faker->firstName;
        $mobile = rand(10000000000, 999999999999);
        $type = User::$types[array_rand(User::$types)];

        $user = User::factory()->create();
        $role = Role::factory()->create();

        $response = $this->patchJson(
            route('users.update', $user),
            [
                'roles' => [$role->getId()],
                User::NATIONAL_CODE => $nationalCode,
                User::FIRST_NAME => $firstName,
                User::MOBILE => $mobile,
                User::TYPE => $type,
            ]
        );
        $response->assertOk();
        $this->assertEquals($nationalCode, $response->getOriginalContent()->{User::NATIONAL_CODE});
        $this->assertEquals($firstName, $response->getOriginalContent()->{User::FIRST_NAME});
        $this->assertEquals($mobile, $response->getOriginalContent()->{User::MOBILE});
        $this->assertEquals($type, $response->getOriginalContent()->{User::TYPE});
        $this->assertTrue($response->getOriginalContent()->roles->contains($role));
    }

    /**
     * @test
     */
    public function userWithoutPermissionCanNotUpdateUser()
    {
        $this->actingAsUser();
        $nationalCode = $this->faker->unique()->randomDigit;
        $firstName = $this->faker->firstName;
        $mobile = rand(10000000000, 999999999999);
        $type = User::$types[array_rand(User::$types)];

        $user = User::factory()->create();
        $this->patchJson(
            route('users.update', $user),
            [
                User::NATIONAL_CODE => $nationalCode,
                User::FIRST_NAME => $firstName,
                User::MOBILE => $mobile,
                User::TYPE => $type,
            ]
        )->assertForbidden();
    }

    /**
     * @test
     */
    public function userWithoutLoggedInCanNotUpdateUser()
    {
        $nationalCode = $this->faker->unique()->randomDigit;
        $firstName = $this->faker->firstName;
        $mobile = rand(10000000000, 999999999999);
        $type = User::$types[array_rand(User::$types)];

        $user = User::factory()->create();
        $response = $this->patchJson(
            route('users.update', $user),
            [
                User::NATIONAL_CODE => $nationalCode,
                User::FIRST_NAME => $firstName,
                User::MOBILE => $mobile,
                User::TYPE => $type,
            ]
        );
        $response->assertUnauthorized();
    }

    /**
     * @test
     */
    public function deleteUser()
    {
        $this->actingAsUserWithPermission(PermissionTitle::DELETE_USER);
        $user = User::factory()->create();
        $this->deleteJson(route('users.destroy', $user));
        $this->assertDatabaseMissing(User::TABLE, [User::ID => $user->{User::ID}]);
    }

    /**
     * @test
     */
    public function userWithoutPermissionCanNotDeleteUser()
    {
        $this->actingAsUser();
        $user = User::factory()->create();
        $this->deleteJson(route('users.destroy', $user))->assertForbidden();
    }

    /**
     * @test
     */
    public function userWithoutLoggedInCanNotDeleteUser()
    {
        $user = User::factory()->create();
        $this->deleteJson(route('users.destroy', $user))->assertUnauthorized();
    }

    /**
     * @test
     */
    public function getAllUserPagination()
    {
        User::factory()->count(9)->create();
        $this->actingAsUserWithPermission(PermissionTitle::GET_ALL_USERS);
        $response = $this->getJson(route('users.index'));
        $response->assertOk();
        $this->assertEquals(10, count($response->getOriginalContent()));
    }

    /**
     * @test
     */
    public function userWithoutPermissionCanNotGetAllUserPagination()
    {
        User::factory()->count(9)->create();
        $this->actingAsUser();
        $this->getJson(route('users.index'))->assertForbidden();
    }

    /**
     * @test
     */
    public function getAllUserPaginationPerpage()
    {
        User::factory()->count(5)->create();
        $this->actingAsUserWithPermission(PermissionTitle::GET_ALL_USERS);
        $response = $this->getJson(route('users.index') . '?per_page=3');
        $response->assertOk();
        $this->assertEquals(3, count($response->getOriginalContent()));
    }

    /**
     * @test
     */
    public function userWithoutPermissionCanNotGetAllUserPaginationPerpage()
    {
        User::factory()->count(5)->create();
        $this->actingAsUser();
        $this->getJson(route('users.index') . '?per_page=3')->assertForbidden();
    }

    /**
     * @test
     */
    public function filterUsersByFirstName()
    {
        User::factory()->count(5)->create();
        $user = User::factory()->create([User::FIRST_NAME => 'testttttt']);
        $this->actingAsUserWithPermission(PermissionTitle::GET_ALL_USERS);
        $response = $this->getJson(route('users.index') . '?firstName=testtt');
        $response->assertOk();
        $this->assertTrue($response->getOriginalContent()->first()->is($user));
    }

    /**
     * @test
     */
    public function userWithoutPermissionCanNotFilterUsersByFirstName()
    {
        User::factory()->count(5)->create();
        $user = User::factory()->create([User::FIRST_NAME => 'testttttt']);
        $this->actingAsUser();
        $this->getJson(route('users.index') . '?firstName=testtt')->assertForbidden();
    }

    /**
     * @test
     */
    public function filterUserByLastName()
    {
        $this->actingAsUserWithPermission(PermissionTitle::GET_ALL_USERS);
        User::factory()->count(7)->create();
        $user = User::factory()->create([User::LAST_NAME => 'tttttttttttttt']);
        $secondUser = User::factory()->create([User::LAST_NAME => 'mmmm']);
        $response = $this->getJson(route('users.index') . '?lastName=ttttttttt');
        $response->assertOk();
        $this->assertEquals(1, count($response->getOriginalContent()));
        $this->assertTrue($response->getOriginalContent()->contains(User::ID, $user->getId()));
        $this->assertFalse($response->getOriginalContent()->contains(User::ID, $secondUser->getId()));
    }

    /**
     * @test
     */
    public function userWithoutPermissionCanNotFilterUserByLastName()
    {
        $this->actingAsUser();
        User::factory()->count(7)->create();
        User::factory()->create([User::LAST_NAME => 'tttttttttttttt']);
        $this->getJson(route('users.index') . '?lastName=ttttttttt')->assertForbidden();
    }

    /**
     * @test
     */
    public function filterUserByNationalCode()
    {
        $this->actingAsUserWithPermission(PermissionTitle::GET_ALL_USERS);
        User::factory()->count(5)->create();
        $user = User::factory()->create([User::NATIONAL_CODE => '1234567891']);
        $secondUser = User::factory()->create([User::NATIONAL_CODE => '1234567892']);
        $response = $this->getJson(route('users.index') . '?nationalCode=1234567891');
        $response->assertOk();
        $this->assertEquals(1, count($response->getOriginalContent()));
        $this->assertTrue($response->getOriginalContent()->contains(User::ID, $user->getId()));
        $this->assertFalse($response->getOriginalContent()->contains(User::ID, $secondUser->getId()));
    }

    /**
     * @test
     */
    public function userWithoutPermissionCanNotfilterUserByNationalCode()
    {
        $this->actingAsUser();
        User::factory()->count(5)->create();
        User::factory()->create([User::NATIONAL_CODE => '1234567891']);
        $this->getJson(route('users.index') . '?nationalCode=1234567891')->assertForbidden();

    }

    /**
     * @test
     */
    public function filterUserByType()
    {
        $this->actingAsUserWithPermission(PermissionTitle::GET_ALL_USERS);
        User::factory()->count(5)->create();
        $user = User::factory()->create([User::TYPE => User::TYPE_COMPANY]);
        $secondUser = User::factory()->create([User::TYPE => User::TYPE_DRIVER]);
        $response = $this->getJson(route('users.index') . '?type=company');
        $response->assertOk();
        $this->assertTrue($response->getOriginalContent()->contains(User::ID, $user->getId()));
        $this->assertFalse($response->getOriginalContent()->contains(User::ID, $secondUser->getId()));
    }

    /**
     * @test
     */
    public function userWithoutPermissionCanNotFilterUserByType()
    {
        $this->actingAsUser();
        User::factory()->count(5)->create();
        User::factory()->create([User::TYPE => User::TYPE_COMPANY]);
        $this->getJson(route('users.index') . '?type=company')->assertForbidden();

    }

    /**
     * @test
     */
    public function filterUserByEmail()
    {
        $this->actingAsUserWithPermission(PermissionTitle::GET_ALL_USERS);
        User::factory()->count(4)->create();
        $user = User::factory()->create([User::EMAIL => 'example@example.com']);
        $secondUser = User::factory()->create([User::EMAIL => 'kjidfjdierkf']);
        $response = $this->getJson(route('users.index') . '?email=xamp');
        $response->assertOk();
        $this->assertEquals(1, count($response->getOriginalContent()));
        $this->assertTrue($response->getOriginalContent()->contains(User::ID, $user->getId()));
        $this->assertFalse($response->getOriginalContent()->contains(User::ID, $secondUser->getId()));
    }

    /**
     * @test
     */
    public function userWithoutPermissionCanNotFilterUserByEmail()
    {
        $this->actingAsUser();
        User::factory()->count(4)->create();
        User::factory()->create([User::EMAIL => 'example@example.com']);
        $this->getJson(route('users.index') . '?email=xamp')->assertForbidden();
    }

    /**
     * @test
     */
    public function filterUserByMobile()
    {
        $this->actingAsUserWithPermission(PermissionTitle::GET_ALL_USERS);
        User::factory()->count(5)->create();
        $user = User::factory()->create([User::MOBILE => '09348488784']);
        $secondUser = User::factory()->create([User::MOBILE => 'mobile']);
        $response = $this->getJson(route('users.index') . '?mobile=09348488784');
        $response->assertOk();
        $this->assertTrue($response->getOriginalContent()->contains(User::ID, $user->getId()));
        $this->assertFalse($response->getOriginalContent()->contains(User::ID, $secondUser->getId()));
    }

    /**
     * @test
     */
    public function userWithoutPermissionCanNotFilterUserByMobile()
    {
        $this->actingAsUser();
        User::factory()->count(5)->create();
        User::factory()->create([User::MOBILE => '09348488784']);
        $this->getJson(route('users.index') . '?mobile=09348488784')->assertForbidden();
    }

    /**
     * @test
     */
    public function filterUserByApproved()
    {
        $this->actingAsUserWithPermission(PermissionTitle::GET_ALL_USERS);
        User::factory()->count(3)->create();
        $user = User::factory()->create([User::APPROVED => 0]);
        $secondUser = User::factory()->create([User::APPROVED => 1]);
        $response = $this->getJson(route('users.index') . '?approved=0');
        $response->assertOk();
        $this->assertTrue($response->getOriginalContent()->contains(User::ID, $user->getId()));
        $this->assertFalse($response->getOriginalContent()->contains(User::ID, $secondUser->getId()));
    }

    /**
     * @test
     */
    public function userWithoutPermissionCanNotFilterUserByApproved()
    {
        $this->actingAsUser();
        User::factory()->count(3)->create();
        User::factory()->create([User::APPROVED => 0]);
        $this->getJson(route('users.index') . '?approved=0')->assertForbidden();
    }

    /**
     * @test
     */
    public function filterUserByCity()
    {
        $this->actingAsUserWithPermission(PermissionTitle::GET_ALL_USERS);
        User::factory()->count(4)->create();
        $user = User::factory()->create([User::CITY => 'kerman']);
        $secondUser = User::factory()->create([User::CITY => 'akarindja']);
        $response = $this->getJson(route('users.index') . '?city=erm');
        $response->assertOk();
        $this->assertTrue($response->getOriginalContent()->contains(User::ID, $user->getId()));
        $this->assertFalse($response->getOriginalContent()->contains(User::ID, $secondUser->getId()));
    }

    /**
     * @test
     */
    public function userWithoutPermissionCanNotFilterUserByCity()
    {
        $this->actingAsUser();
        User::factory()->count(4)->create();
        User::factory()->create([User::CITY => 'kerman']);
        $this->getJson(route('users.index') . '?city=erm')->assertForbidden();
    }

    /**
     * @test
     */
    public function filterUserByRegion()
    {
        $this->actingAsUserWithPermission(PermissionTitle::GET_ALL_USERS);
        User::factory()->count(5)->create();
        $user = User::factory()->create([User::REGION => 'isfahan']);
        $secondUser = User::factory()->create([User::REGION => 'shirzzzzz']);
        $response = $this->getJson(route('users.index') . '?region=han');
        $response->assertOk();
        $this->assertTrue($response->getOriginalContent()->contains(User::ID, $user->getId()));
        $this->assertFalse($response->getOriginalContent()->contains(User::ID, $secondUser->getId()));
    }

    /**
     * @test
     */
    public function userWithoutPermissionCanNotFilterUserByRegion()
    {
        $this->actingAsUser();
        User::factory()->count(5)->create();
        User::factory()->create([User::REGION => 'isfahan']);
        $this->getJson(route('users.index') . '?region=han')->assertForbidden();
    }
}
