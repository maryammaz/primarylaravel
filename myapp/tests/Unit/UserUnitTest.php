<?php

namespace Tests\Unit;

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Tests\TestCase;
use App\Interfaces\Models\UserInterface;

class UserUnitTest extends TestCase
{
    /**
     * @test
     */
    public function testCreateUser()
    {
        $user = User::factory()->make();
        $createdUser = User::createObject(
            $user->{User::NATIONAL_CODE},
            $user->{User::FIRST_NAME},
            $user->{User::MOBILE},
            $user->{User::TYPE},
            $user->{User::LAST_NAME},
            $user->{User::APPROVED},
            $user->{User::PASSWORD},
            $user->{User::ADDRESS},
            $user->{User::CITY},
            $user->{User::EMAIL},
            $user->{User::CHARGE},
            $user->{User::SUBSCRIPTION_EXPIRE},
            $user->{User::REGION},
            $user->{User::PHONE}
        );
        $this->assertTrue($createdUser instanceof UserInterface);
        $this->assertEquals($createdUser->{User::FIRST_NAME}, $user->{User::FIRST_NAME});
        $this->assertEquals($createdUser->{User::LAST_NAME}, $user->{User::LAST_NAME});
        $this->assertEquals($createdUser->{User::MOBILE}, $user->{User::MOBILE});
    }

    /**
     * @test
     */
    public function updateUser()
    {
        $user = User::factory()->create();
        $nationalCode = (string)$user->getNationalCode();
        $approved = $this->faker->boolean;
        $mobile = (string)rand(10000000000, 999999999999);
        $firstName = $this->faker->firstName;
        $lastName = $this->faker->lastName;
        $type = User::$types[array_rand(User::$types)];
        $email = $this->faker->email;
        $charge = (string)$this->faker->randomDigit;
        $subscription_expire = $this->faker->date();
        $password = '123';
        $city = $this->faker->city;
        $region = $this->faker->countryISOAlpha3;
        $address = $this->faker->address;
        $phoneNumber = (string)rand(10000000000, 999999999999);
        $createdUser = $user->updateObject(
            $nationalCode,
            $firstName,
            $mobile,
            $type,
            $lastName,
            $approved,
            $password,
            $address,
            $city,
            $email,
            $charge,
            $subscription_expire,
            $region,
            $phoneNumber
        );

        $this->assertTrue($createdUser instanceof UserInterface);
        $this->assertDatabaseHas(
            User::TABLE,
            [
                User::ID => $user->getId(),
                User::NATIONAL_CODE => (string)$nationalCode,
                User::EMAIL => $email,
                User::MOBILE => (string)$mobile,
                User::FIRST_NAME => $firstName,
                User::LAST_NAME => $lastName,
                User::TYPE => $type,
                User::APPROVED => $approved,
                User::CHARGE => (string)$charge,
                User::SUBSCRIPTION_EXPIRE => $subscription_expire,
                User::PASSWORD => $password,
                User::CITY => $city,
                User::REGION => $region,
                User::ADDRESS => $address,
                User::PHONE => (string)$phoneNumber,
            ]
        );
    }

    /**
     * @test
     */
    public function checkHasPermission()
    {
        /** @var User $user */
        $user = User::factory()->create();
        $permission = Permission::factory()->create();
        $role=Role::factory()->create();
        $user->roles()->attach($role);
        $role->permissions()->attach($permission);
        $this->assertTrue($user->hasPermission($permission->getTitle()));
        $secondPermission = Permission::factory()->create();
        $this->assertFalse($user->hasPermission($secondPermission));
    }
}
